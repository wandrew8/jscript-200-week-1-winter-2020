/**
 * MATH
 */
// 1. Pagliacci charges $16.99 for a 13” pizza and $19.99 for a 17” pizza.
// What is the area for each of these pizzas?
// (radius would be the listed size - i.e. 13" - divided by 2)
const largePizzaArea = (Math.PI * ((17 / 2) ** 2 )).toFixed(2);
const smallPizzaArea = (Math.PI * ((13 / 2) ** 2)).toFixed(2)


// 2. What is the cost per square inch of each pizza?
const costPerInchLarge = (19.99 / largePizzaArea).toFixed(2);
const costPerInchSmall = (16.99 / smallPizzaArea).toFixed(2);
console.log(`A large pizza costs $${costPerInchLarge} per inch.`);
console.log(`A small pizza costs $${costPerInchSmall} per inch.`);


// 3. Using the Math object, put together a code snippet
// that allows you to draw a random card with a value
// between 1 and 13 (assume ace is 1, jack is 11…)
const randomNum1 = Math.floor((Math.random() * 13) + 1);
const randomNum2 = Math.floor((Math.random() * 13) + 1);
const randomNum3 = Math.floor((Math.random() * 13) + 1);


// 4. Draw 3 cards and use Math to determine the highest
// card
const highestNum = Math.max(randomNum1, randomNum2, randomNum3)
console.log(`The highest number is ${highestNum}`)


/**
 * ADDRESS LINE
 */

// 1. Create variables for firstName, lastName,
// streetAddress, city, state, and zipCode. Use
// this information to create a formatted address block
// that could be printed onto an envelope.
const firstName = 'Andrew';
const lastName = 'Weiss';
const streetAddress = '2102 12st NE';
const city = 'Seattle';
const state = 'WA';
const zip = '98021';

const fullAddress = `${firstName} ${lastName}
${streetAddress}
${city}, ${state} ${zip}`

console.log(fullAddress);


// 2. You are given a string in this format:
// firstName lastName(assume no spaces in either)
// streetAddress
// city, state zip(could be spaces in city and state)
// Write code that is able to extract the first name from this string into a variable.
// Hint: use indexOf, slice, and / or substring
const addressArray = fullAddress.split(' ');
const firstNameFromAddress = addressArray[0];
console.log(firstNameFromAddress)

const firstSpaceIndex = fullAddress.indexOf(' ');
const extractedFirstName = fullAddress.substring(0, firstSpaceIndex)
console.log(extractedFirstName)


/**
 * FIND THE MIDDLE DATE
 */
// On your own find the middle date(and time) between the following two dates:
// 1/1/2020 00:00:00 and 4/1/2020 00:00:00
//
// Look online for documentation on Date objects.

const startDate = new Date(2020, 0, 1).getTime();
const endDate = new Date(2020, 3, 1).getTime();
const middleDate = new Date((startDate + endDate) / 2 );
console.log(middleDate)

